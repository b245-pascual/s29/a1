    //s29

//1. find users with letter s in their firstName and letter d in their lastName.
//use the $or operator and show only the firstName and lastName fields hide the _id field.
        
        db.users.find({
                $or: [
                {
                        firstName: { $regex: "s", $options: "$i"}
                },
                {
                        lastName: { $regex: "d", $options: "$i"}
                }]
        },{
                _id: 0,
                firstName: 1,
                lastName: 1
        });



//2. find users who are from  HR dept. and their age is greater than or equal to 70
        db.users.find({
                $and:[{
                        department: "HR",
                        age: {$gte: 70}
                }]
        })

        
       
        
//3. find users with the letter e in their firstName and has age of lessthan or equal to 30.
        db.users.find({
                $and: [{
                        firstName: {$regex: "e"},
                        age: {$lte:30}
                }]
        })